module gitlab.com/al-coder/aloptions

go 1.21.4

replace gitlab.com/al-coder/xservice => ../xservice

require gitlab.com/al-coder/xservice v0.0.0

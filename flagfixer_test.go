package aloptions

import (
	"os"
	"testing"
)

func TestFlagParseRemoveHelp(t *testing.T) {
	for _, v := range []struct {
		args []string
		want error
	}{
		{[]string{""}, nil},
		{[]string{"-v --v  --v  -v "}, nil},
		{[]string{"", "", ""}, nil},
		{[]string{"-v", " --v ", "--v"}, nil},
		{[]string{"", "", ""}, nil},
		{[]string{"-Dev -path='tst'"}, nil},
		{[]string{"--Dev --path='tst'"}, nil},
		{[]string{" -Dev   -path='tst'"}, nil},
		{[]string{" --Dev   --path='tst'"}, nil},
		{[]string{" -Dev   -path='tst' "}, nil},
		{[]string{" --Dev   --path='tst' "}, nil},
		{[]string{"-Dev", "-path='tst'"}, nil},
		{[]string{"--Dev", "--path='tst'"}, nil},
		{[]string{" -Dev ", "-path='tst' "}, nil},
		{[]string{" --Dev ", "--path='tst' "}, nil},
		{[]string{"-h"}, ErrFlagParse},
		{[]string{"-h -v"}, ErrFlagParse},
		{[]string{"-v -h"}, ErrFlagParse},
		{[]string{" -v  -h "}, ErrFlagParse},
		{[]string{" --v  --h "}, ErrFlagParse},
		{[]string{"-Build", "-help"}, ErrFlagParse},
		{[]string{" -Build ", " -help "}, ErrFlagParse},
		{[]string{"--Build", "--help "}, ErrFlagParse},
		{[]string{" --Build ", " --help "}, ErrFlagParse},
		{[]string{"-help", "-Build"}, ErrFlagParse},
		{[]string{" -help ", " -Build "}, ErrFlagParse},
		{[]string{"--help ", "--Build"}, ErrFlagParse},
		{[]string{" --help ", " --Build "}, ErrFlagParse},
		{[]string{"-h --help  -help  --h -path='tst'"}, ErrFlagParse},
	} {
		os.Args = append(os.Args[:1], v.args...)
		got := FlagParseRemoveHelp()
		if got != v.want {
			t.Errorf("FlagParseRemoveHelp(%q) == %v, want %v", v.args, got, v.want)
		}
	}
}

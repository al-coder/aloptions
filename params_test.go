package aloptions

import (
	"testing"
	"time"

	"gitlab.com/al-coder/xservice"
)

// - ParamString -

func TestParamString_GetTags(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamString
		pStruct any
		want    string
		wErr    error
	}{
		{&sp.Host, nil, "", ErrArgumentNotValid},
		{&sp.TestString2, sp, "", nil},
		{&sp.Host, sp, `tag:"main-host-h" default:"localhost:8888"`, nil},
	} {
		got, err := v.pField.GetTags(v.pStruct)

		showError := func() {
			t.Errorf("%T.GetTags(%T) == %q, %v. Want %q, %v", v.pField, v.pStruct, got, err, v.want, v.wErr)
		}

		if got != v.want {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

func TestParamString_GetTag(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamString
		pStruct any
		tagName string
		want    string
		wErr    error
	}{
		{&sp.Host, nil, "", "", ErrArgumentEmpty},
		{&sp.Host, nil, "SSDSDSD", "", ErrArgumentNotValid},
		{&sp.Host, sp, "", "", ErrArgumentEmpty},
		{&sp.Host, sp, "XXX", "", ErrWrongTag},
		{&sp.Host, sp, "tag", "main-host-h", nil},
	} {
		got, err := v.pField.GetTag(v.pStruct, v.tagName)

		showError := func() {
			t.Errorf("%T.GetTag(%T, %s) == %q, %v. Want %q, %v", v.pField, v.pStruct, v.tagName, got, err, v.want, v.wErr)
		}

		if got != v.want {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

func TestParamString_Get(t *testing.T) {
	sp := initSP()
	want := "localhost:8888"
	if got := sp.Host.Get(); got != want {
		t.Errorf("sp.Host.Get() == %q, want %q", got, want)
	}
}

func TestParamString_Set(t *testing.T) {
	sp := initSP()
	want := "kuku"
	sp.Host.Set(want)
	if got := sp.Host.Get(); got != want {
		t.Errorf("sp.Host.Get() == %q, want %q", got, want)
	}
}

func TestParamString_GetTagDetailed(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamString
		pStruct any
		tagName string
		wCmd    []string
		wArg    []string
		wErr    error
	}{
		{&sp.Host, nil, "", nil, nil, ErrArgumentEmpty},
		{&sp.Host, sp, "", nil, nil, ErrArgumentEmpty},
		{&sp.Host, sp, "pag", nil, nil, ErrWrongTag},
		{&sp.Host, sp, "tag", []string{"main"}, []string{"host", "h"}, nil},
	} {
		gotCmd, gotArg, err := v.pField.GetTagDetailed(v.pStruct, v.tagName)

		showError := func() {
			t.Errorf("%T.GetTagDetailed(%T, %s) == %q, %q, %v. Want %q, %q, %v", v.pField, v.pStruct, v.tagName, gotCmd, gotArg, err, v.wCmd, v.wArg, v.wErr)
		}

		if !xservice.CompareArrays(gotCmd, v.wCmd) || !xservice.CompareArrays(gotArg, v.wArg) {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

// - ParamBool -

func TestParamBool_GetTags(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamBool
		pStruct any
		want    string
		wErr    error
	}{
		{&sp.WorkAsDev, nil, "", ErrArgumentNotValid},
		{&sp.WorkAsDev, sp, `tag:"-workAsDev" default:"false"`, nil},
	} {
		got, err := v.pField.GetTags(v.pStruct)

		showError := func() {
			t.Errorf("%T.GetTags(%T) == %q, %v. Want %q, %v", v.pField, v.pStruct, got, err, v.want, v.wErr)
		}

		if got != v.want {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

func TestParamBool_GetTag(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamBool
		pStruct any
		tagName string
		want    BoolTag
		wErr    error
	}{
		{&sp.WorkAsDev, nil, "", BoolTag{false, ""}, ErrArgumentEmpty},
		{&sp.WorkAsDev, nil, "SSDSDSD", BoolTag{false, ""}, ErrArgumentNotValid},
		{&sp.WorkAsDev, sp, "", BoolTag{false, ""}, ErrArgumentEmpty},
		{&sp.WorkAsDev, sp, "XXX", BoolTag{false, ""}, ErrWrongTag},
		{&sp.WorkAsDev, sp, "tag", BoolTag{true, "-workAsDev"}, nil},
		{&sp.WorkAsDev, sp, "default", BoolTag{false, "false"}, nil},
	} {
		got, err := v.pField.GetTag(v.pStruct, v.tagName)

		showError := func() {
			t.Errorf("%T.GetTag(%T, %s) == %v, %v. Want %v, %v", v.pField, v.pStruct, v.tagName, got, err, v.want, v.wErr)
		}

		if got != v.want {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

func TestParamBool_Get(t *testing.T) {
	sp := initSP()
	want := true
	if got := sp.WorkAsDev.Get(); got != want {
		t.Errorf("sp.WorkAsDev.Get() == %v, want %v", got, want)
	}
}

func TestParamBool_Set(t *testing.T) {
	sp := initSP()
	want := false
	sp.WorkAsDev.Set(want)
	if got := sp.WorkAsDev.Get(); got != want {
		t.Errorf("sp.WorkAsDev.Get() == %v, want %v", got, want)
	}
}

func TestParamBool_GetTagDetailed(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamBool
		pStruct any
		tagName string
		wCmd    []string
		wArg    []string
		wErr    error
	}{
		{&sp.WorkAsDev, nil, "", nil, nil, ErrArgumentEmpty},
		{&sp.WorkAsDev, sp, "", nil, nil, ErrArgumentEmpty},
		{&sp.WorkAsDev, sp, "pag", nil, nil, ErrWrongTag},
		{&sp.WorkAsDev, sp, "tag", []string{""}, []string{"workAsDev"}, nil},
		{&sp.WorkAsDev, sp, "default", []string{"false"}, nil, nil},
	} {
		gotCmd, gotArg, err := v.pField.GetTagDetailed(v.pStruct, v.tagName)

		showError := func() {
			t.Errorf("%T.GetTagDetailed(%T, %s) == %v, %v, %v. Want %v, %v, %v", v.pField, v.pStruct, v.tagName, gotCmd, gotArg, err, v.wCmd, v.wArg, v.wErr)
		}

		if !xservice.CompareArrays(gotCmd, v.wCmd) || !xservice.CompareArrays(gotArg, v.wArg) {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

// - ParamTDur -

func TestParamTDur_GetTags(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamTDur
		pStruct any
		want    string
		wErr    error
	}{
		{&sp.Timeouts.MainHandler, nil, "", ErrArgumentNotValid},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, `tag:"-MainHandler" default:"9s"`, nil},
	} {
		got, err := v.pField.GetTags(v.pStruct)

		showError := func() {
			t.Errorf("%T.GetTags(%T) == %q, %v. Want %q, %v", v.pField, v.pStruct, got, err, v.want, v.wErr)
		}

		if got != v.want {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

func TestParamTDur_GetTag(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamTDur
		pStruct any
		tagName string
		want    TDurTag
		wErr    error
	}{
		{&sp.Timeouts.MainHandler, nil, "", TDurTag{time.Duration(0), ""}, ErrArgumentEmpty},
		{&sp.Timeouts.MainHandler, nil, "SSDSDSD", TDurTag{time.Duration(0), ""}, ErrArgumentNotValid},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, "", TDurTag{time.Duration(0), ""}, ErrArgumentEmpty},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, "XXX", TDurTag{time.Duration(0), ""}, ErrWrongTag},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, "tag", TDurTag{time.Duration(0), "-MainHandler"}, xservice.ErrAny},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, "default", TDurTag{9 * time.Second, "9s"}, nil},
	} {
		got, err := v.pField.GetTag(v.pStruct, v.tagName)

		showError := func() {
			t.Errorf("%T.GetTag(%T, %s) == %v, %v. Want %v, %v", v.pField, v.pStruct, v.tagName, got, err, v.want, v.wErr)
		}

		if got != v.want {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

func TestParamTDur_Get(t *testing.T) {
	sp := initSP()
	want := true
	if got := sp.WorkAsDev.Get(); got != want {
		t.Errorf("sp.WorkAsDev.Get() == %v, want %v", got, want)
	}
}

func TestParamTDur_Set(t *testing.T) {
	sp := initSP()
	want := false
	sp.WorkAsDev.Set(want)
	if got := sp.WorkAsDev.Get(); got != want {
		t.Errorf("sp.WorkAsDev.Get() == %v, want %v", got, want)
	}
}

func TestParamTDur_GetTagDetailed(t *testing.T) {
	sp := initSP()
	for _, v := range []struct {
		pField  *ParamTDur
		pStruct any
		tagName string
		wCmd    []string
		wArg    []string
		wErr    error
	}{
		{&sp.Timeouts.MainHandler, nil, "", nil, nil, ErrArgumentEmpty},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, "", nil, nil, ErrArgumentEmpty},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, "pag", nil, nil, ErrWrongTag},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, "tag", []string{""}, []string{"MainHandler"}, xservice.ErrAny},
		{&sp.Timeouts.MainHandler, &sp.Timeouts, "default", []string{"9s"}, nil, nil},
	} {
		gotCmd, gotArg, err := v.pField.GetTagDetailed(v.pStruct, v.tagName)

		showError := func() {
			t.Errorf("%T.GetTagDetailed(%T, %s) == %v, %v, %v. Want %v, %v, %v", v.pField, v.pStruct, v.tagName, gotCmd, gotArg, err, v.wCmd, v.wArg, v.wErr)
		}

		if !xservice.CompareArrays(gotCmd, v.wCmd) || !xservice.CompareArrays(gotArg, v.wArg) {
			showError()
		} else if err != v.wErr && !xservice.InsideWraped(v.wErr, err) {
			showError()
		}
	}
}

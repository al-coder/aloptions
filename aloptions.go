package aloptions

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	"gitlab.com/al-coder/xservice"
)

type FlagFunc func() error // ErrMustExit indicates the need to stop further parsing and exit the App

type TFlagRegister struct {
	cmd       []string     // array because could be several options: "help", "h"
	arg       []string     // array because could be several options: "-verbose", "-v"
	kind      reflect.Kind // type
	valueBool *bool
	valueInt  *int
	valueTDur *time.Duration
	valueStr  *string
	hint      string   // description will be shown on "help" command
	fnc       FlagFunc // function will be executed if not nil. NB: don't "os.Exit()" on "help", just return 'true'
}

type Options struct {
	FlagRegister []TFlagRegister // registered in this applications flags
	clFlags      *Flags          // flags which were received from command line
}

func NewOptions() *Options {
	o := &Options{}
	o.clFlags = NewFlags()
	return o
}

func (o *Options) Close() {
	if o.FlagRegister != nil {
		clear(o.FlagRegister)
	}
	if o.clFlags != nil {
		o.clFlags.Close()
	}
}

func (o *Options) setValue(k int, flagVal string) error {
	switch o.FlagRegister[k].kind {
	case reflect.Bool:
		*o.FlagRegister[k].valueBool = true
	case reflect.String:
		*o.FlagRegister[k].valueStr = flagVal
	case reflect.Int:
		if i, err := strconv.Atoi(flagVal); err == nil {
			*o.FlagRegister[k].valueInt = i
		} else {
			return xservice.WrapErr(o.setValue, []any{k, flagVal}, err, "can't set integer flag")
		}
	case 101: //time.Duration
		if td, err := time.ParseDuration(flagVal); err == nil {
			*o.FlagRegister[k].valueTDur = time.Duration(td)
		} else {
			return xservice.WrapErr(o.setValue, []any{k, flagVal}, err, "can't set time.Duration flag")
		}
	default:
		return xservice.WrapErr(o.setValue, []any{k, flagVal}, ErrWrongType, "")
	}

	if o.FlagRegister[k].fnc != nil {
		return o.FlagRegister[k].fnc()
	}
	return nil
}

// ParseFlags parses flags which were received from command line.
func (o *Options) ParseFlags() error {
	// Parse command line
	if err := o.clFlags.Parse(); err != nil {
		return xservice.WrapErr(o.ParseFlags, nil, err, "")
	}

	// Set values
	for k, v := range o.FlagRegister {
		cmd := o.clFlags.FindCommand(v.cmd)
		if strings.Join(v.arg, "") == "" { //it's a command
			if cmd >= 0 { // command found
				flagVal := Flags(*o.clFlags)[cmd].Value
				if err := o.setValue(k, flagVal); err != nil {
					return xservice.WrapErr(o.ParseFlags, nil, err, "")
				}
			}
		} else { //it's a command's arg
			if cmd >= 0 { // command found
				if p, ok := o.clFlags.FindArg(cmd, v.arg); ok {
					if err := o.setValue(k, *p); err != nil {
						return xservice.WrapErr(o.ParseFlags, nil, err, "")
					}
				}
			}
		}
	}

	return nil
}

// Flag registration, the analog of flag.Bool
func (o *Options) RegFlagBool(cmd, arg []string, defValue bool, helpHint string) *bool {
	return o.RegFlagBoolFnc(cmd, arg, defValue, helpHint, nil)
}

// Flag registration, the analog of flag.Bool
func (o *Options) RegFlagBoolFnc(cmd, arg []string, defValue bool, helpHint string, fnc FlagFunc) *bool { //No more 5 params principe broken purposely.
	fR := TFlagRegister{
		cmd:  cmd,
		arg:  arg,
		kind: reflect.Bool,
		hint: helpHint,
		fnc:  fnc,
	}
	fR.valueBool = new(bool)
	*fR.valueBool = defValue
	o.FlagRegister = append(o.FlagRegister, fR)
	return fR.valueBool //o.FlagRegister[len(o.FlagRegister)-1].valueBool
}

// Flag registration, the analog of flag.String
func (o *Options) RegFlagStr(cmd, arg []string, defValue string, helpHint string) *string {
	fR := TFlagRegister{
		cmd:  cmd,
		arg:  arg,
		kind: reflect.String,
		hint: helpHint,
	}
	fR.valueStr = new(string)
	*fR.valueStr = defValue
	o.FlagRegister = append(o.FlagRegister, fR)
	return fR.valueStr //o.FlagRegister[len(o.FlagRegister)-1].valueStr
}

// Flag registration, the analog of flag.Int
func (o *Options) RegFlagInt(cmd, arg []string, defValue int, helpHint string) *int {
	fR := TFlagRegister{
		cmd:  cmd,
		arg:  arg,
		kind: reflect.Int,
		hint: helpHint,
	}
	fR.valueInt = new(int)
	*fR.valueInt = defValue
	o.FlagRegister = append(o.FlagRegister, fR)
	return fR.valueInt //o.FlagRegister[len(o.FlagRegister)-1].valueInt
}

// Flag registration, Time Duration, for example "6s" = 6 seconds.
func (o *Options) RegFlagTDur(cmd, arg []string, defValue time.Duration, helpHint string) *time.Duration {
	fR := TFlagRegister{
		cmd:  cmd,
		arg:  arg,
		kind: 101,
		hint: helpHint,
	}
	fR.valueTDur = new(time.Duration)
	*fR.valueTDur = defValue
	o.FlagRegister = append(o.FlagRegister, fR)
	return fR.valueTDur //o.FlagRegister[len(o.FlagRegister)-1].valueTDur
}

// Provides pointer to a string which contains information for printing HELP CLI command.
func (o *Options) SprintDefHelp() *string {
	res := ""
	var prevCmd []string = nil
	for _, v := range o.FlagRegister {
		if xservice.CompareArrays[string](prevCmd, v.cmd) {
			res += "\n"
			prevCmd = v.cmd
		}

		cmds := strings.Join(v.cmd, " OR ")
		args := strings.Join(v.arg, " OR -")
		if len(args) > 0 {
			args = "-" + args
		}
		s := fmt.Sprintf("\t%s\t%s\t%s\n", cmds, args, v.hint)
		res += s
	}
	return &res
}

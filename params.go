package aloptions

import (
	"strings"
	"time"

	"gitlab.com/al-coder/xservice"
)

// # ParamString - to use in a way of string in structs to work with tags.
type ParamString string

// Returns all struct tags as a string
func (h *ParamString) GetTags(pStruct any) (string, error) {
	return GetStrucTagValues(pStruct, h, "")
}

// Returns the value of a specific struct tag
func (h *ParamString) GetTag(pStruct any, tagName string) (string, error) {
	if tagName == "" {
		return "", ErrArgumentEmpty
	}
	return GetStrucTagValues(pStruct, h, tagName)
}

// 'Getter'
func (h *ParamString) Get() string {
	return string(*h)
}

// 'Setter'
func (h *ParamString) Set(s string) {
	*h = ParamString(s)
}

// Cutting given Tag on a Command and Params
// Format:
//
//	[command]-[argument-[argument...]]
func (h *ParamString) GetTagDetailed(pStruct any, tagName string) (cmd []string, arg []string, err error) {
	s, err := h.GetTag(pStruct, tagName)
	if err != nil {
		return nil, nil, xservice.WrapErr(h.GetTagDetailed, []any{pStruct, tagName}, err, "")
	}

	if strings.TrimSpace(s) == "" {
		return nil, nil, nil
	}

	cmd, arg = getTagDetailed(s)
	return cmd, arg, nil
}

func getTagDetailed(tag string) (cmd []string, arg []string) {
	if i := strings.Index(tag, "-"); i != -1 { // [command]-[argument]
		cmd = []string{tag[0:i]}
		arg := strings.Split(tag[i+1:], "-")
		return cmd, arg
	} else { // it's the command
		return []string{tag}, nil
	}
}

// # ParamBool - to use in a way of bool in structs to work with tags.
type ParamBool bool

// Returns all struct tags as a string
func (h *ParamBool) GetTags(pStruct any) (string, error) {
	return GetStrucTagValues(pStruct, h, "")
}

type BoolTag struct {
	Bool bool
	Str  string
}

// Returns particular struct tag
// Because it's a 'flag', any value except empty and explicit `false` counts 'true'.
func (h *ParamBool) GetTag(pStruct any, tagName string) (BoolTag, error) {
	if tagName == "" {
		return BoolTag{false, ""}, ErrArgumentEmpty
	}

	s, err := GetStrucTagValues(pStruct, h, tagName)
	if err != nil {
		return BoolTag{false, ""}, xservice.WrapErr(h.GetTag, []any{pStruct, tagName}, err, "")
	}

	if s == "" || strings.ToLower(s) == "false" {
		return BoolTag{false, s}, nil
	}

	return BoolTag{true, s}, nil
	/*if b, err := strconv.ParseBool(s); err != nil {
		return BoolTag{false, s}, xservice.WrapErr(h.GetTag, []any{pStruct, tagName}, err, "")
	} else {
		return BoolTag{b, s}, nil
	}*/
}

// 'Getter'
func (h *ParamBool) Get() bool {
	return bool(*h)
}

// 'Setter'
func (h *ParamBool) Set(b bool) {
	*h = ParamBool(b)
}

// Cutting given Tag on a Command and Params
// Format:
//
//	[command]-[argument-[argument...]]
func (h *ParamBool) GetTagDetailed(pStruct any, tagName string) (cmd []string, arg []string, err error) {
	bTag, err := h.GetTag(pStruct, tagName)
	if err != nil {
		return nil, nil, xservice.WrapErr(h.GetTagDetailed, []any{pStruct, tagName}, err, "")
	}

	if strings.TrimSpace(bTag.Str) == "" {
		return nil, nil, nil
	}

	cmd, arg = getTagDetailed(bTag.Str)
	return cmd, arg, nil
}

// # ParamTDur - to use in a way of time.Duration in structs to work with tags.
type ParamTDur time.Duration

// Returns all struct tags as a string
func (h *ParamTDur) GetTags(pStruct any) (string, error) {
	return GetStrucTagValues(pStruct, h, "")
}

type TDurTag struct {
	TDur time.Duration
	Str  string
}

// Returns all struct tags as a string
func (h *ParamTDur) GetTag(pStruct any, tagName string) (TDurTag, error) {
	if tagName == "" {
		return TDurTag{time.Duration(0), ""}, ErrArgumentEmpty
	}

	s, err := GetStrucTagValues(pStruct, h, tagName)
	if err != nil {
		return TDurTag{time.Duration(0), ""}, xservice.WrapErr(h.GetTag, []any{pStruct, tagName}, err, "")
	}

	td, err := time.ParseDuration(s)
	if err != nil {
		return TDurTag{time.Duration(0), s}, xservice.WrapErr(h.GetTag, []any{pStruct, tagName}, err, "")
	}

	return TDurTag{td, s}, nil
}

// 'Getter'
func (h *ParamTDur) Get() time.Duration {
	return time.Duration(*h)
}

// 'Setter'
func (h *ParamTDur) Set(b time.Duration) {
	*h = ParamTDur(b)
}

// Cutting given Tag on a Command and Params
// Format:
//
//	[command]-[argument-[argument...]]
func (h *ParamTDur) GetTagDetailed(pStruct any, tagName string) (cmd []string, arg []string, err error) {
	tDurRag, err := h.GetTag(pStruct, tagName)
	if err != nil && !strings.Contains(err.Error(), "invalid duration") { // Error, but not time.'invalid time duration'
		return nil, nil, xservice.WrapErr(h.GetTagDetailed, []any{pStruct, tagName}, err, "")
	}

	if strings.TrimSpace(tDurRag.Str) == "" {
		return nil, nil, xservice.WrapErr(h.GetTagDetailed, []any{pStruct, tagName}, err, "")
	}

	cmd, arg = getTagDetailed(tDurRag.Str)
	return cmd, arg, xservice.WrapErr(h.GetTagDetailed, []any{pStruct, tagName}, err, "")
}

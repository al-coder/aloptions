package aloptions

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/al-coder/xservice"
)

// # Flags
// In aloptions terminology flag can be a command or an argument.
// There could be n commands. Each command can consist of n arguments. Where n = 0 -> maximum number of arguments in a command line.
// Arguments' marks "-" and "--" are treated equally.
//
// Example:
//
//	app.exe version -v=true
//
//	//here: 1st argument is a command 'version'
//	//command 'version' contain 1 argument, which consists of key=value
//
//	go help -v=true --verbose"
//	//here: "go" - application, "help" - command, "v" - argument of "help" command with value "true", "verbose" - argument of "help" command.
//	//All of them are flags. All of them are keys with or without values (including the command).

// # Flags implements command line flags:
type Flag struct {
	Id    int // direct simple increment. // TODO: Carries redundant information, CmdId shell address to # in the Flags array.
	CmdId int // id of command for args, -1 for commands.
	Key   string
	Value string
	Args  []Arg //nil for Arg
}
type Arg struct {
	Key   *string
	Value *string
}

type Flags []Flag

func NewFlags() *Flags {
	return &Flags{}
}

// Parses command line flags. Analog of flag.Parse()
func (f *Flags) Parse() error {
	lastCmdId := -1
	id := -1

	rawStr := []string{}
	defer func() { clear(rawStr) }()

	if len(os.Args) == 0 {
		return nil
	}

	first := os.Args[0]
	exe, err := os.Executable()
	if err == nil {
		first = strings.TrimPrefix(first, exe) // because the first 'os.argument' also could contain 'args'.
	}
	if first != "" {
		if !strings.Contains(first, `"`) {
			firsts := strings.Split(first, " ")
			if len(firsts) > 1 {
				rawStr = append(rawStr, firsts[1:]...)
			}
		} else {
			rawStr = append(rawStr, first)
		}

		if len(rawStr) > 0 {
			if strings.Contains(rawStr[0], "/") {
				if len(rawStr) > 1 {
					rawStr = append([]string{}, rawStr[1:]...)
				} else {
					rawStr = []string{}
				}
			}
		}
	}

	for _, v := range os.Args[1:] {
		rawStr = append(rawStr, strings.Split(v, " ")...)
	}

	for _, v := range rawStr {
		v = strings.TrimSpace(v)
		if v == "" {
			continue
		}
		kv := strings.Split(v, "=")
		key := strings.TrimSpace(kv[0])
		prefix := strings.HasPrefix(key, "-")
		if prefix {
			key = strings.TrimLeft(key, "-")
		}
		value := ""
		if len(kv) == 2 {
			value = strings.TrimSpace(kv[1])
		}
		if (key == "") || (len(kv) > 2) {
			return fmt.Errorf("%sflag reading error: %q", xservice.FuncFullnameColon(f.Parse), kv)
		}

		f.addFlag(&id, &lastCmdId, prefix, key, value)
		//fmt.Println(key, value)
	}

	return nil
}

func (f *Flags) Close() {
	clear(*f)
}

func (f *Flags) addFlag(id, lastCmdId *int, prefix bool, key, value string) { //DRY was broken purposely.
	flag := Flag{}
	if prefix { //arg
		if *lastCmdId == -1 {
			f.addFlag(id, lastCmdId, false, "", "")
		}
		*id++
		flag.Id = *id
		flag.CmdId = *lastCmdId
		flag.Key = key
		flag.Value = value
		Flags(*f)[*lastCmdId].Args = append(Flags(*f)[*lastCmdId].Args, Arg{&flag.Key, &flag.Value})
	} else { //command
		*id++
		flag.Id = *id
		flag.CmdId = -1
		flag.Key = key
		flag.Value = value
		*lastCmdId = *id
	}
	*f = append(*f, flag)
}

// Finds Flag by field "keys". Returns either # in slice either -1 if not found.
func (f *Flags) Find(keys []string) int {
	for k, v := range *f {
		if xservice.Inn(v.Key, &keys, true) {
			return k
		}
	}
	return -1
}

// Finds Command's Flag by field "keys". Returns either # in slice either -1 if not found.
func (f *Flags) FindCommand(keys []string) int {
	for k, v := range *f {
		if xservice.Inn(v.Key, &keys, true) && (v.CmdId == -1) {
			return k
		}
	}
	return -1
}

// Finds all Arguments which belong to the Command. Returns nil if list of Args is empty.
func (f *Flags) FindArgs(cmd int) *[]Arg {
	i := len(*f)
	if cmd >= 0 && cmd < i {
		if Flags(*f)[cmd].CmdId == -1 { //it's a command which can consist from Args
			return &Flags(*f)[cmd].Args
		}
	}

	return &[]Arg{}
}

// Finds Arguments inside the Command by field "keys".
func (f *Flags) FindArg(cmd int, keys []string) (*string, bool) {
	for _, v := range *f.FindArgs(cmd) {
		if xservice.Inn[string](*v.Key, &keys, true) {
			return v.Value, true
		}
	}

	return nil, false
}

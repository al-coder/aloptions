package aloptions

import (
	"os"
	"reflect"
	"testing"
)

func TestYamlRead(t *testing.T) {
	resCorrect := &TServerParams{
		TestBool:    true,
		TestString:  "",
		TestString2: "",                             //ParamString
		TestInt:     777,                            //`tag:"-testInt" default:"777" hint:"help info"`
		Host:        "localhost:9999",               //ParamString `tag:"main-host-h" default:"localhost:8888"`
		WorkAsDev:   true,                           //ParamBool   `tag:"-workAsDev" default:"false"` // Because it's a 'flag', any value except empty and explicit `false` counts 'true'.
		Timeouts:    TTimeouts{1000000000, 0, 0, 0}, //TTimeouts   `tag:"-Timeouts"`
	}

	type TArgs struct {
		pp any
		fn string
	}
	tests := []struct {
		name    string
		args    TArgs
		wantRes any
		wantErr error
	}{
		// TODO: Add test cases.
		{"1", TArgs{nil, ""}, nil, nil},
		{"2", TArgs{&TServerParams{}, ""}, &TServerParams{}, os.ErrNotExist},
		{"3", TArgs{&TServerParams{}, "/mnt/nonfile.non"}, &TServerParams{}, os.ErrNotExist},
		{"4", TArgs{&TServerParams{}, "./yaml_test.yaml"}, resCorrect, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := YamlRead(tt.args.pp, tt.args.fn)
			if err != tt.wantErr {
				t.Errorf("YamlRead() error = %v, wantErr %v", err, tt.wantErr)
			} else if !reflect.DeepEqual(tt.args.pp, tt.wantRes) {
				t.Errorf("YamlRead() returned = %v,\n want %v", tt.args.pp, tt.wantRes)
			}
		})
	}
}

package aloptions

import (
	"errors"
	"os"
	"strings"

	"gitlab.com/al-coder/xservice"
)

// Flag Fixer-------------------------------------------------------------
// Issue: flag.Parse() uses os.Exit(n) once encounters any of the variants of the help flag.
// os.Exit(n) in it's turn:
//
//	"os: Exit does not respect deferred functions making any cleanup code useless"
//	#38261: https://github.com/golang/go/issues/38261

var ErrFlagParse = errors.New("help flag was encountered")

// # FlagParseRemoveHelp
// Use FlagParseRemoveHelp() before any calls of flag.Parse().
// If any of the variants of the help flag were encountered, it returns ErrFlagParse and remove such flags.
// So you can call flag.PrintDefaults() in case of ErrFlagParse. Or flag.Parse() if result is nil.
func FlagParseRemoveHelp() (res error) {
	res = nil
	newArgs := os.Args[:1]
	as := []string{"-h", "--h", "-help", "--help"}
	if len(os.Args) > 1 {
		for _, v := range os.Args[1:] {
			var nap []string
			ap := strings.Split(v, " ")
			for _, vv := range ap {
				vvTrimed := strings.TrimSpace(vv)
				if xservice.Inn(strings.ToLower(vvTrimed), &as) {
					res = ErrFlagParse
				} else {
					if vvTrimed != "" {
						nap = append(nap, vv)
					}
				}
			}
			s := strings.Join(nap, " ")
			newArgs = append(newArgs, s)
		}
	}
	os.Args = newArgs
	return res
}

package aloptions

import (
	"fmt"
	"testing"

	"gitlab.com/al-coder/xservice"
)

func TestErrMustExit_ExitCode(t *testing.T) {
	const errMsg = "tst"
	const errCode = 0
	var err error = NewErrMustExit(errMsg, errCode)
	if gotErr, ok := err.(*ErrMustExit); !ok || fmt.Sprint(gotErr) != errMsg || gotErr.ExitCode() != errCode {
		t.Errorf("ErrMustExit.ExitCode(%s, %d) == %v, %v", errMsg, errCode, gotErr, ok)
	}

}

func TestDewrapAsErrMustExit(t *testing.T) {
	wrapper := func(err error) error {
		return xservice.WrapErr(nil, nil, err, "")
	}

	e := xservice.ErrAny
	e = wrapper(e)
	eOk := NewErrMustExit("new", 7)

	for _, v := range []struct {
		in    error
		want  bool
		wCode int
	}{
		{nil, false, -1},
		{fmt.Errorf("new"), false, -1},
		{eOk, true, 7},
		{wrapper(e), false, -1},
		{wrapper(wrapper(e)), false, -1},
		{wrapper(eOk), true, 7},
		{wrapper(wrapper(eOk)), true, 7},
		{wrapper(wrapper(wrapper(eOk))), true, 7},
	} {
		if got, gotOk := DewrapAsErrMustExit(v.in); gotOk != v.want {
			t.Errorf("DewrapAsErrMustExit(%v) == %v. Want: %v", v.in, gotOk, v.want)
		} else if got != nil {
			if gotExitCode := got.ExitCode(); gotExitCode != v.wCode {
				t.Errorf("DewrapAsErrMustExit(%v).ExitCode() == %d. Want: %d", v.in, gotExitCode, v.wCode)
			}
		}
	}
}

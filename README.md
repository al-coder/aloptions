# aloptions 
https://gitlab.com/al-coder/aloptions

## Version
version="0.1.1-alpha"

## Description
aloptions: 
    reads flags:
        supports:
            subcommand (params)
        support types:
            string, bool
        supports: short & long names, descriptions
        supports: special events like help and version by providing function - callback
        supports: Go struct tags
        supports: yaml files (flags always has a priority) // TODO: implement!

## Functions
// # Flags
// In aloptions terminology flag can be a command or an argument.
// There could be n commands. Each command can consist of n arguments. Where n = 0 -> maximum number of arguments in a command line.
// Arguments' marks "-" and "--" are treated equally.
//
// Example:
//
//	app.exe version -v=true
//
//	//here: 1st argument is a command 'version'
//	//command 'version' contain 1 argument, which consists of key=value
//
//	go help -v=true --verbose"
//	//here: "go" - application, "help" - command, "v" - argument of "help" command with value "true", "verbose" - argument of "help" command.
//	//All of them are flags. All of them are keys with or without values (including the command).


// Issue: flag.Parse() uses os.Exit(n) once encounters any of the variants of the help flag.
// os.Exit(n) in it's turn:
//
//	"os: Exit does not respect deferred functions making any cleanup code useless"
//	#38261: https://github.com/golang/go/issues/38261
//
// # FlagParseRemoveHelp
// Use FlagParseRemoveHelp() before any calls of flag.Parse().
// If any of the variants of the help flag were encountered, it returns ErrFlagParse and remove such flags.
// So you can call flag.PrintDefaults() in case of ErrFlagParse. Or flag.Parse() if result is nil.

## Example
```
yourapp.exe help --verbose
```

```
```

## TODO
    structtags.go -> separate pckg
    remove old comments

## Acknowledgments
+ Under inspiration of: "github.com/ilyakaznacheev/cleanenv"

## Contribution
Let's do it together.

<!--
## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->

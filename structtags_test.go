package aloptions

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/al-coder/xservice"
)

type TTimeouts struct {
	MainHandler  ParamTDur     `tag:"-MainHandler" default:"9s"`
	ReadTimeout  time.Duration `tag:"-ReadTimeout" default:"7s"`
	WriteTimeout time.Duration `tag:"-WriteTimeout" default:"8s" hint:"help info"`
	IdleTimeout  time.Duration `tag:"-IdleTimeout" default:"60s"`
}

type TServerParams struct {
	TestBool    bool
	TestString  string
	TestString2 ParamString
	TestInt     int         `tag:"-testInt" default:"777" hint:"help info"`
	Host        ParamString `tag:"main-host-h" default:"localhost:8888"`
	WorkAsDev   ParamBool   `tag:"-workAsDev" default:"false"` // Because it's a 'flag', any value except empty and explicit `false` counts 'true'.
	WorkAsDev2  ParamBool   `tag:"-workAsDev2" default:"true"` // Because it's a 'flag', any value except empty and explicit `false` counts 'true'.
	Timeouts    TTimeouts   `tag:"-Timeouts"`
}

func initSP() *TServerParams {
	return &TServerParams{
		TestInt:    777,
		Host:       "localhost:8888",
		WorkAsDev:  true,
		WorkAsDev2: true,
		Timeouts: TTimeouts{
			MainHandler: ParamTDur(25 * time.Second),
		},
	}
}

func TestSetStructFieldValues(t *testing.T) { // _
}

func TestSetStructFieldValuesWTaginfo(t *testing.T) {
	//case1
	sp1 := &TServerParams{}
	if err := SetStructFieldValuesWTaginfo(sp1); err != nil {
		t.Error(err)
	}
	cs1 := initSP()
	if sp1.TestInt != cs1.TestInt || sp1.Host != cs1.Host || sp1.WorkAsDev2 != cs1.WorkAsDev2 {
		t.Errorf("SetStructFieldValuesWTaginfo() == %v, want %v", sp1, cs1)
	}

	//case2
	var sp2 *int
	if err := SetStructFieldValuesWTaginfo(sp2); !xservice.InsideWraped(ErrNotStruct, err) {
		t.Errorf("SetStructFieldValuesWTaginfo() got error: %v, want error: %v", err, ErrNotStruct)
	}

}

func TestGetStrucTagValues(t *testing.T) {
	//case1
	cs1 := initSP()
	if s, err := GetStrucTagValues(cs1, &cs1.Host, ""); err != nil || s != `tag:"main-host-h" default:"localhost:8888"` {
		t.Errorf("GetStrucTagValues() == %s, %v, want %s, %v", s, err, cs1.Host, nil)
	}

	//case2
	var sp2 *int
	if _, err := GetStrucTagValues(sp2, nil, ""); !xservice.InsideWraped(ErrArgumentNotValid, err) {
		t.Errorf("GetStrucTagValues() got error: %v, want error: %v", err, ErrArgumentNotValid)
	}

	//case3
	cs3 := initSP()
	if s, err := GetStrucTagValues(cs3, &cs3.Host, "tag"); err != nil || s != "main-host-h" {
		t.Errorf("GetStrucTagValues() == %s, %v, want %s, %v", s, err, cs1.Host, nil)
	}

}

func TestGetStructTagKeyAndValue(t *testing.T) {
	//case1
	sp1 := &TServerParams{}
	if _, err := GetStructTagKeyAndValue(sp1, "tag", "hint"); err != nil {
		t.Error(err)
	}

	//case2
	var sp2 *int
	if _, err := GetStructTagKeyAndValue(sp2, "tag", "hint"); !xservice.InsideWraped(ErrNotStruct, err) {
		t.Errorf("GetStructTagKeyAndValue() got error: %v, want error: %v", err, ErrNotStruct)
	}
}

func TestStruct_String(t *testing.T) {
	type args struct {
		structFieldEval TStructFieldEval
	}
	tests := []struct {
		name string
		pp   any
		args args
		want bool
	}{
		// TODO: Add test cases.
		{"1", initSP(), args{nil}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res, err := NewStruct(tt.pp)
			if err != nil {
				t.Errorf("Struct() error: %v", err)
			}

			got := res.String(tt.args.structFieldEval)
			if (got != "") != tt.want {
				t.Errorf("Struct.String() = %v, want %v", got, tt.want)
			}

			fmt.Println(got)
		})
	}
}

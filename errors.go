package aloptions

import (
	"errors"

	"gitlab.com/al-coder/xservice"
)

var (
	ErrNotStruct        error = errors.New("got not a struct as a parameter")
	ErrWrongType        error = errors.New("argument of wrong type")
	ErrArgumentEmpty    error = errors.New("argument can't be empty")
	ErrArgumentNotValid error = errors.New("argument is not valid")
	ErrWrongTag         error = errors.New("wrong tag")
)

const (
// ErrExitNoStdout = iota //Absent or Bad StdOut during Help command execution
)

type IErrMustExit interface {
	Error() string
	ExitCode() int
}

type ErrMustExit struct {
	code int
	msg  string
}

func (e *ErrMustExit) Error() string {
	return e.msg
}

func (e *ErrMustExit) ExitCode() int {
	return e.code
}

func NewErrMustExit(msg string, code int) IErrMustExit {
	return &ErrMustExit{
		msg:  msg,
		code: code,
	}
}

// DewrapAsErrMustExit is unwrapping the err in a cycle until it is the ErrMustExit.
// In case of success, it returns (*ErrMustExit and true). Otherwise, (nil and false).
func DewrapAsErrMustExit(err error) (IErrMustExit, bool) {
	if err == nil { // nil means ok (not an error), so it's NOT inside.
		return nil, false
	}

	for {
		if err, ok := err.(IErrMustExit); ok {
			return err, true
		}

		switch t := err.(type) {
		case xservice.WrError:
			err = t.Unwrap()
		default:
			return nil, false // Can't go deeper, failed in dewrap.
		}
	}
}

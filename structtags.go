package aloptions

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	"gitlab.com/al-coder/xservice"
)

type (
	KeyAndValue struct {
		key, value string
		nesting    KeyAndValues
	}

	KeyAndValues []KeyAndValue
)

func (h KeyAndValues) String() (res string) {
	return String(h)
}

func String(h KeyAndValues, nestLevel ...int) (res string) {
	nl := 0
	if len(nestLevel) > 0 {
		if nestLevel[0] > 0 {
			nl = nestLevel[0]
		}
	}

	maxKeyLength := 0
	for _, v := range h {
		maxKeyLength = max(maxKeyLength, len(v.key))
	}

	for _, v := range h {
		s := strings.Repeat("\t", nl) +
			v.key + strings.Repeat(" ", maxKeyLength-len(v.key)) +
			":" + xservice.Iff(v.value == "", "<>", v.value)

		if res == "" {
			res = res + s
		} else {
			res = res + "\n" + s
		}

		if v.nesting != nil {
			res = res + "\n" + String(v.nesting, nl+1)
		}
	}

	return res
}

type (
	TStructFieldEval func(f reflect.StructField, v reflect.Value) KeyAndValue
)

func defStructFieldEvalTag(f reflect.StructField, v reflect.Value) KeyAndValue {
	res := f.Tag.Get("default")
	return KeyAndValue{key: "", value: res}
}

func setStructFieldValues(structValue reflect.Value, structType reflect.Type, structFieldEval TStructFieldEval) error {
	const bts string = "bad tag structure"
	const btsiv string = bts + ": Int value"

	setFieldValue := func(f reflect.StructField) error {
		// A Value can be changed only if it is addressable and was not obtained by the use of unexported struct fields.
		if fieldValue := structValue.FieldByName(f.Name); fieldValue.IsValid() && fieldValue.CanSet() {
			if tagValue := structFieldEval(f, fieldValue).value; tagValue != "" {
				// change value of N
				switch fieldValue.Kind() {
				case reflect.String:
					fieldValue.SetString(tagValue)
				case reflect.Bool:
					tv, err := strconv.ParseBool(tagValue)
					if err != nil {
						return xservice.WrapErr(setStructFieldValues, []any{structValue, structType}, errors.New(bts), "")
					}
					fieldValue.SetBool(tv)
				case reflect.Int:
					tv, err := strconv.Atoi(tagValue)
					if err != nil {
						return xservice.WrapErr(setStructFieldValues, []any{structValue, structType}, errors.New(btsiv), "")
					}
					if fieldValue.OverflowInt(int64(tv)) {
						return xservice.WrapErr(setStructFieldValues, []any{structValue, structType}, errors.New(btsiv), "")
					}
					fieldValue.SetInt(int64(tv))
				case reflect.Int64: // including time.Duration
					tv, err := time.ParseDuration(tagValue)
					if err != nil {
						return xservice.WrapErr(setStructFieldValues, []any{structValue, structType}, errors.New(bts+": time format"), "")
					}
					fieldValue.SetInt(int64(tv))
				default:
					return xservice.WrapErr(setStructFieldValues, []any{structValue, structType}, errors.New(bts), "")
				}
			}
		}
		return nil
	}

	for i := 0; i < structType.NumField(); i++ {
		structField := structType.Field(i)
		//fmt.Printf("Feild - %s, ", structField.Name) //DEBUG:
		if structField.Type.Kind() == reflect.Struct {
			//fmt.Println(": struct") //DEBUG:
			fieldValue := structValue.FieldByName(structField.Name)
			setStructFieldValues(fieldValue, structField.Type, structFieldEval)
		} else {
			//fmt.Println(": not struct") //DEBUG:
			if err := setFieldValue(structField); err != nil {
				return xservice.WrapErr(setStructFieldValues, []any{structValue, structType}, err, structField.Name)
			}
		}
	}
	return nil
}

// # SetStructFieldValues reads the struct and fills it's fields' values with a result of injected function structFieldEval
//
// Example of struct:
//
//	type TTimeouts struct {
//		ReadTimeout  time.Duration `tag:"ReadTimeout" default:"7s"`
//		WriteTimeout time.Duration `tag:"WriteTimeout" default:"8s"`
//	}
//
//	type TServerParams struct {
//		Host       string    `tag:"host" default:"localhost:8888"`
//		WorkAsDev  bool      `tag:"workAsDev" default:"true"`
//		Timeouts   TTimeouts `tag:"Timeouts"`
//	}
func SetStructFieldValues(pStruct any, structFieldEval TStructFieldEval) error {
	if structFieldEval == nil {
		structFieldEval = defStructFieldEvalTag
	}

	// NB: Check 'err' and 'val' after calling this injection as it uses variables scope of outer function.
	/*structFieldEvalTagV := func(f reflect.StructField, v reflect.Value) KeyAndValue { // ignoring KeyAndValue in this case
		if v.Addr().Interface() == fV.Addr().Interface() {
			if tagName == "" {
				val = fmt.Sprint(f.Tag)
			} else {
				sTag, ok := f.Tag.Lookup(tagName)
				val = fmt.Sprint(sTag)
				err = xservice.Iff[error](ok, nil, ErrWrongTag)
			}
		}
		return KeyAndValue{}
	}*/

	p, err := NewStruct(pStruct)
	if err != nil {
		return err
	}

	return setStructFieldValues(p.structValue, p.structType, structFieldEval)

	/*
	   structValue := reflect.ValueOf(pp)

	   	if !structValue.IsValid() {
	   		return xservice.WrapErr(SetStructFieldValues, []any{pp}, ErrArgumentNotValid, "")
	   	}

	   structVElem := structValue.Elem()

	   	if i := structVElem.Kind(); i == reflect.Struct {
	   		structType := reflect.TypeOf(pp).Elem()
	   		return setStructFieldValues(structVElem, structType, structFieldEval)
	   	} else {

	   		return xservice.WrapErr(SetStructFieldValues, nil, ErrNotStruct, "Kind():"+fmt.Sprint(int(i)))
	   	}
	*/
}

// # SetStructFieldValuesWTaginfo reads the structure tag field 'default' from the given pointer to the struct instance and fills with this value the corresponding field.
//
// Example of struct:
//
//	type TTimeouts struct {
//		ReadTimeout  time.Duration `tag:"ReadTimeout" default:"7s"`
//		WriteTimeout time.Duration `tag:"WriteTimeout" default:"8s"`
//	}
//
//	type TServerParams struct {
//		Host       string    `tag:"host" default:"localhost:8888"`
//		WorkAsDev  bool      `tag:"workAsDev" default:"true"`
//		Timeouts   TTimeouts `tag:"Timeouts"`
//	}
func SetStructFieldValuesWTaginfo(pp any) error {
	return SetStructFieldValues(pp, nil)
}

// # GetStrucTagValues returns struct tags of the given struct field.
// Use tagName to specify the tag. Use Use tagName == "" to get all tag data.
//
// Example:
//
//	type TServerParams struct {
//			Host       string    `tag:"host" default:"localhost:8888"`
//	}
//
//	SP := &TServerParams{}
//	v, err := alo.GetStrucTagValues(SP, &SP.Host, "") // v = `tag:"host" default:"localhost:8888"`
func GetStrucTagValues(pStruct any, pStructField any, tagName string) (val string, err error) {
	fV := reflect.ValueOf(pStructField)
	if !fV.IsValid() {
		return "", xservice.WrapErr(GetStrucTagValues, []any{pStructField}, ErrArgumentNotValid, "")
	}

	if kind := fV.Kind(); kind == reflect.Interface || kind == reflect.Pointer {
		fV = fV.Elem()
	}

	// NB: Check 'err' and 'val' after calling this injection as it uses variables scope of outer function.
	structFieldEvalTagV := func(f reflect.StructField, v reflect.Value) KeyAndValue { // ignoring KeyAndValue in this case
		if v.Addr().Interface() == fV.Addr().Interface() {
			if tagName == "" {
				val = fmt.Sprint(f.Tag)
			} else {
				sTag, ok := f.Tag.Lookup(tagName)
				val = fmt.Sprint(sTag)
				err = xservice.Iff[error](ok, nil, ErrWrongTag)
			}
		}
		return KeyAndValue{}
	}

	p, err := NewStruct(pStruct)
	if err != nil {
		return "", err
	}

	structEvalKeyValue(p.structValue, p.structType, structFieldEvalTagV)
	return

	/*
		// old realization
		sV := reflect.ValueOf(pStruct)
		if !sV.IsValid() {
			return "", xservice.WrapErr(GetStrucTagValues, []any{pStruct}, ErrArgumentNotValid, "")
		}

		structVElem := sV.Elem()
		if k := structVElem.Kind(); k != reflect.Struct {
			return "", xservice.WrapErr(GetStrucTagValues, []any{pStruct, pStructField}, ErrNotStruct, "Kind():"+fmt.Sprint(int(k)))
		}

		for i := 0; i < structVElem.NumField(); i++ {
			valueField := structVElem.Field(i)
			if valueField.Addr().Interface() == fieldVElem.Addr().Interface() {
				if tagName == "" {
					sTags := structVElem.Type().Field(i).Tag
					return fmt.Sprint(sTags), nil
				} else {
					sTag, ok := structVElem.Type().Field(i).Tag.Lookup(tagName)
					return fmt.Sprint(sTag), xservice.Iff[error](ok, nil, ErrWrongTag)
				}
			}
		}
		return "", xservice.WrapErr(GetStrucTagValues, []any{pStruct, pStructField}, nil, "the StructField doesn't belong to the Struct")
	*/
}

// # GetStructTagKeyAndValue reads two struct field tags from the given pointer to the struct instance and fills with them the array of strings.
//
// Example of struct:
//
//	type TTimeouts struct {
//		ReadTimeout  time.Duration `tag:"-ReadTimeout" hint:"ReadTimeout"`
//	}
//
//	type TServerParams struct {
//			Host       string    `tag:"host" hint:"IP:port"`
//			Timeouts   TTimeouts `tag:"Timeouts"`
//	}
//
//	res, err := GetStructTagKeyAndValue(&TServerParams{}, "tag", "hint") // returned array will be: {{"host", "IP:port"}, {"Timeouts", ""}, {"-ReadTimeout", "ReadTimeout"}}
func GetStructTagKeyAndValue(pp any, tagKey, tagValue string) (KeyAndValues, error) {
	structFieldEvalTagKV := func(f reflect.StructField, v reflect.Value) KeyAndValue {
		tk := f.Tag.Get(tagKey)
		tv := f.Tag.Get(tagValue)
		return KeyAndValue{key: tk, value: tv}
	}

	p, err := NewStruct(pp)
	if err != nil {
		return nil, err
	}

	return p.StructEvalKeyValue(structFieldEvalTagKV), nil
}

type Struct struct {
	structValue reflect.Value
	structType  reflect.Type
}

// NewStruct returns the instance of Struct and nil if success, otherwise nil and error.
func NewStruct(pp any) (*Struct, error) { //NB: Dry was broken as Go authors separated Value and Type.
	refValue := reflect.ValueOf(pp)
	if !refValue.IsValid() {
		return nil, ErrArgumentNotValid
	}

	if kind := refValue.Kind(); kind == reflect.Interface || kind == reflect.Pointer {
		refValue = refValue.Elem()
	}

	if kind := refValue.Kind(); kind != reflect.Struct {
		return nil, xservice.WrapErr(nil, nil, ErrNotStruct, "Kind():"+fmt.Sprint(int(kind)))
	}

	refType := reflect.TypeOf(pp)
	if kind := refType.Kind(); kind == reflect.Interface || kind == reflect.Pointer {
		refType = refType.Elem()
	}

	if refType == nil {
		return nil, xservice.WrapErr(nil, nil, errors.New("wrong type"), "")
	}

	return &Struct{refValue, refType}, nil
}

// # String prints the giving struct into the resulting string.
func (h *Struct) String(structFieldEval TStructFieldEval) string {
	kv := h.StructEvalKeyValue(structFieldEval)
	return kv.String()
}

func defStructFieldEvalKV(f reflect.StructField, v reflect.Value) KeyAndValue {
	s := ""
	switch f.Type.Kind() {
	case reflect.String:
		s = v.String()
	case reflect.Bool:
		s = fmt.Sprint(v.Bool())
	case reflect.Int:
		s = fmt.Sprint(v.Int())
	case reflect.Int64: // including time.Duration
		s = fmt.Sprint(v.Int())
	}
	return KeyAndValue{key: f.Name, value: s}
}

func structEvalKeyValue(structValue reflect.Value, structType reflect.Type, structFieldEval TStructFieldEval) (index KeyAndValues) {
	if structFieldEval == nil {
		structFieldEval = defStructFieldEvalKV
	}

	for i := 0; i < structType.NumField(); i++ {
		structField := structType.Field(i)
		//fmt.Printf("Field - %s, ", structField.Name) //DEBUG:
		fieldValue := structValue.Field(i)
		kv := structFieldEval(structField, fieldValue)
		if structField.Type.Kind() == reflect.Struct {
			kv.nesting = structEvalKeyValue(fieldValue, structField.Type, structFieldEval)
		}
		index = append(index, kv)
	}
	return index
}

// # StructEvalKeyValue fills the result array with data based on logic from structFieldEval.
func (h *Struct) StructEvalKeyValue(structFieldEval TStructFieldEval) (index KeyAndValues) {
	return structEvalKeyValue(h.structValue, h.structType, structFieldEval)
}

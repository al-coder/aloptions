package aloptions

import (
	"bufio"
	"os"
	"reflect"
	"strings"
)

// loadYamlFile load the YAML file into the key-value map
func loadYamlFile(fn string) (map[string]string, error) {
	m := make(map[string]string)

	file, err := os.Open(fn)
	if err != nil {
		return m, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		str := scanner.Text()
		//remove YAML comments after '#'
		if i := strings.Index(str, "#"); i > -1 {
			str = str[0:i]
		}

		//take value after separator ':'
		if i := strings.Index(str, ":"); i > -1 {
			key := str[0:i]
			key = strings.TrimSpace(strings.ToLower(key))
			if key != "" {
				val := str[i+1:]
				val = strings.TrimSpace(val)
				val = strings.Trim(val, `"`)
				val = strings.TrimSpace(val)
				m[key] = val
			}
		}
	}

	return m, nil
}

// # YamlRead reads the YAML file using the giving in 'fn' full path and fills with its data the giving in the 'pp' pointer to the instance of the struct.
// It returns os.ErrNotExist in case of any errors upon reading the file.
//
// YamlRead uses the struct field's value of the tag 'tag' as the name of the key to look up in the YAML file.
// If the tag is empty or absent, YamlRead uses field's name.
//
// In a YAML file YamlRead treats inlined fields as plain, so no matter how many inlined levels exist, key names should not be repeated.
// YamlRead also ignores tabulation as well as extra spaces at the beginning of a string as a mark of inlining and treats all strings as one plain map.
//
// On both sides YamlRead is not case-sensitive.
//
// The separator ':' is mandatory for empty values in a YAML file. An empty value can be a beneficiary to overwrite a previous value of a field.
//
// Example of the YAML file:
//
//	key: #value is empty.
//	other_key #this key will be ignored.
//
// Example of the struct:
//
//	type TTimeouts struct {
//		ReadTimeout  time.Duration `tag:"ReadTimeout" default:"7s"`
//		WriteTimeout time.Duration `tag:"WriteTimeout" default:"8s"`
//	}
//
//	type TServerParams struct {
//		Host       string    `tag:"host" default:"localhost:8888"`
//		WorkAsDev  bool      `tag:"workAsDev" default:"true"`
//		Timeouts   TTimeouts `tag:"Timeouts"`
//	}
func YamlRead(pp any, fn string) error {
	if pp == nil {
		return nil
	}
	ym, err := loadYamlFile(fn) //error of reading file
	if err != nil {
		return os.ErrNotExist
	}
	defer clear(ym)
	if len(ym) == 0 {
		return nil
	}

	fieldEval := func(f reflect.StructField, v reflect.Value) KeyAndValue {
		fName := f.Tag.Get("tag")
		if fName == "" {
			fName = f.Name
		}
		fName = strings.Trim(strings.TrimSpace(fName), "-")
		fName = strings.ToLower(fName)
		if fName != "" {
			return KeyAndValue{key: "", value: ym[fName]}
		}
		return KeyAndValue{key: "", value: ""}
	}

	return SetStructFieldValues(pp, fieldEval)
}

package aloptions

import (
	"os"
	"testing"
)

func TestNewFlags(t *testing.T) {
	f := NewFlags()
	defer f.Close()
	if f == nil {
		t.Error("NewFlags returned nil")
	}
}

func TestFlags_Close(t *testing.T) { // _
}

func TestFlags_Parse(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()

	for _, v := range []struct {
		in   []string
		want int
	}{
		{in: []string{}, want: 0},
		{[]string{""}, 0},
		{[]string{"server"}, 0},
		{[]string{"/mnt/server"}, 0},
		{[]string{"/mnt/server version"}, 1},
		{[]string{"/mnt/server -help"}, 2},
		{[]string{"/mnt/server --h"}, 2},
		{[]string{"/mnt/server  version "}, 1},
		{[]string{"/mnt/server    -help   "}, 2},
		{[]string{"/mnt/server     --h     "}, 2},
		{[]string{"/mnt/server", "version"}, 1},
		{[]string{"/mnt/server", "-help"}, 2},
		{[]string{"/mnt/server", "--h"}, 2},
		{[]string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt'"}, 7},
		{[]string{"/mnt/server", "-dev -listen='127.0.0.1:8844'", "-file-server-dir='../www'", "-client-app-dir='../client-app'", "-private-key='../certificate/private.key'", "-public-key='../certificate/public.crt'"}, 7},
	} {
		os.Args = v.in
		f := NewFlags()
		defer f.Close()
		err := f.Parse()
		if err != nil {
			t.Error(err)
		}
		if i := len(*f); i != v.want {
			t.Errorf("f.Parse() == %v, want: %v", i, v.want)
		}
		//fmt.Println(f)
	}
}

func TestFlags_Find(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()

	for _, v := range []struct {
		keys []string
		in   []string
		want int
	}{
		{nil, []string{}, -1},
		{[]string{}, nil, -1},
		{[]string{}, []string{}, -1},
		{nil, nil, -1},
		{[]string{""}, []string{""}, -1},
		{[]string{"ersio"}, []string{"/mnt/server version"}, -1},
		{[]string{"version"}, []string{"/mnt/server version"}, 0},
		{[]string{"server"}, []string{"/mnt/server -help"}, -1},
		{[]string{"/mnt/server"}, []string{"/mnt/server -help"}, -1},
		{[]string{"help"}, []string{"/mnt/server -help"}, 1},      //first would be empty command
		{[]string{"hElp"}, []string{"/mnt/server -VeR -helP"}, 2}, //first would be empty command
		{[]string{"shelps"}, []string{"/mnt/server -help"}, -1},
		{[]string{"help"}, []string{"/mnt/server -shelps"}, -1},
		{[]string{"help"}, []string{"/mnt/server shelps"}, -1},
		{[]string{"H"}, []string{"/mnt/server --h"}, 1},
		{[]string{"deV"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt'"}, 1},
		{[]string{"Listen"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844'", "-file-server-dir='../www'", "-client-app-dir='../client-app'", "-private-key='../certificate/private.key'", "-public-key='../certificate/public.crt'"}, 2},
		{[]string{"public-key"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt'"}, 6},
		{[]string{"public-key"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844'", "-file-server-dir='../www'", "-client-app-dir='../client-app'", "-private-key='../certificate/private.key'", "-public-key='../certificate/public.crt'"}, 6},
		{[]string{"d", "e", "v"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt'"}, -1},
		{[]string{"file", "SERVER", "Dev"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt'"}, 1},
	} {
		os.Args = v.in
		f := NewFlags()
		defer f.Close()
		err := f.Parse()
		if err != nil {
			t.Error(err)
		}

		if i := f.Find(v.keys); i != v.want {
			t.Errorf("f.Find(%q) == %v, want: %v", v.keys, i, v.want)
		}
	}
}

func TestFlags_FindCommand(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()

	for _, v := range []struct {
		keys []string
		in   []string
		want int
	}{
		{nil, []string{}, -1},
		{[]string{}, nil, -1},
		{[]string{}, []string{}, -1},
		{nil, nil, -1},
		{[]string{""}, []string{""}, -1},
		{[]string{"ersio"}, []string{"/mnt/server version"}, -1},
		{[]string{"version"}, []string{"/mnt/server version"}, 0},
		{[]string{"version"}, []string{"/mnt/server -version"}, -1},
		{[]string{"v"}, []string{"/mnt/server --v"}, -1},
		{[]string{"server"}, []string{"/mnt/server -help"}, -1},
		{[]string{"/mnt/server"}, []string{"/mnt/server -help"}, -1},
		{[]string{"help"}, []string{"/mnt/server -help"}, -1},
		{[]string{"hElp"}, []string{"/mnt/server -VeR -helP"}, -1},
		{[]string{"hElp"}, []string{"/mnt/server -VeR helP"}, 2}, //0 - is a virtual command for '-ver', 1 - is '-ver' itself, so 2
		{[]string{"shelps"}, []string{"/mnt/server -help"}, -1},
		{[]string{"help"}, []string{"/mnt/server -shelps"}, -1},
		{[]string{"help"}, []string{"/mnt/server shelps"}, -1},
		{[]string{"H"}, []string{"/mnt/server --h"}, -1},
		{[]string{"H"}, []string{"/mnt/server h"}, 0},
		{[]string{"h"}, []string{"/mnt/server H"}, 0},
		{[]string{"cMd"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' CmD='5' -private-key='../certificate/private.key' -public-key='../certificate/public.crt'"}, 5},
		{[]string{"cMd"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' CmD=5 -private-key='../certificate/private.key' -public-key='../certificate/public.crt'"}, 5},
		{[]string{"file", "SERVER", "cmd"}, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt' cmd"}, 7},
	} {
		os.Args = v.in
		f := NewFlags()
		defer f.Close()
		err := f.Parse()
		if err != nil {
			t.Error(err)
		}

		if i := f.FindCommand(v.keys); i != v.want {
			t.Errorf("f.FindCommand(%q) == %v, want: %v", v.keys, i, v.want)
		}
	}
}

func TestFlags_FindArgs(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()

	for _, v := range []struct {
		cmd  int
		in   []string
		want int
	}{
		{-1, []string{}, 0},
		{0, []string{}, 0},
		{0, []string{"/mnt/server version"}, 0},
		{0, []string{"/mnt/server version -v -sss='sss'"}, 2},
		{2, []string{"/mnt/server version -v cmd -sss='sss'"}, 1},
		{3, []string{"/mnt/server -version -v cmd -sss='sss'"}, 1},
		{5, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' CmD='5' -private-key='../certificate/private.key' -public-key='../certificate/public.crt'"}, 2},
		{5, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' CmD='5'"}, 0},
		{0, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt' cmd"}, 6},
		{7, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt' cmd -v"}, 1},
		{7, []string{"/mnt/server", "-dev -listen='127.0.0.1:8844' -file-server-dir='../www' -client-app-dir='../client-app' -private-key='../certificate/private.key' -public-key='../certificate/public.crt' cmd"}, 0},
	} {
		os.Args = v.in
		f := NewFlags()
		defer f.Close()
		err := f.Parse()
		if err != nil {
			t.Error(err)
		}

		if i := len(*f.FindArgs(v.cmd)); i != v.want {
			t.Errorf("f.FindArgs(%d) == %d, want: %v", v.cmd, i, v.want)
		}
	}
}

func TestFlags_FindArg(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()

	for _, v := range []struct {
		cmd  int
		key  []string
		in   []string
		want string
	}{
		{-1, nil, []string{}, ""},
		{-1, []string{""}, []string{}, ""},
		{0, []string{""}, []string{}, ""},
		{0, []string{""}, []string{"/mnt/server version"}, ""},
		{0, []string{"ver"}, []string{"/mnt/server version"}, ""},

		{0, []string{"version"}, []string{"/mnt/server sversions"}, ""},
		{0, []string{"sversions"}, []string{"/mnt/server version"}, ""},
		{0, []string{"vErsion"}, []string{"/mnt/server version"}, ""},
		{0, []string{"Version"}, []string{"/mnt/server versiOn"}, ""},

		{0, []string{"VER", "Help", "VERSION"}, []string{"/mnt/server help"}, ""},
		{0, []string{"VER", "VERSION", "helP"}, []string{"/mnt/server help"}, ""},
		{0, []string{"hElp", "VER", "VERSION"}, []string{"/mnt/server heLp"}, ""},

		{0, []string{"VER", "Help", "VERSION"}, []string{"/mnt/server help -helP"}, ""},
		{0, []string{"VER", "Help", "VERSION"}, []string{"/mnt/server help -helP='good'"}, "'good'"},
		{0, []string{"VER", "Help", "VERSION"}, []string{"/mnt/server help -helP=5"}, "5"},

		{2, []string{"VER", "VERSION", "lisTen"}, []string{"/mnt/server cmd -v help -Listen='127.0.0.1:8844' -file-server-dir='../www'"}, "'127.0.0.1:8844'"},
		{2, []string{"l", "VER", "VERSION"}, []string{"/mnt/server -ver heLp --L='127.0.0.1:8844' -file-server-dir='../www'"}, "'127.0.0.1:8844'"},
		{2, []string{"VER", "VERSION", "liSten"}, []string{"/mnt/server", "cmd -v", "help -listeN='127.0.0.1:8844'", "-file-server-dir='../www'"}, "'127.0.0.1:8844'"},
		{2, []string{"L", "VER", "VERSION"}, []string{"/mnt/server", "-ver", "heLp", "-file-server-dir='../www'", "--l='127.0.0.1:8844'"}, "'127.0.0.1:8844'"},
	} {
		os.Args = v.in
		f := NewFlags()
		defer f.Close()
		err := f.Parse()
		if err != nil {
			t.Error(err)
		}

		if ps, ok := f.FindArg(v.cmd, v.key); ok {
			if *ps != v.want {
				t.Errorf("f.FindArg(%d, %v) == %q, want: %q", v.cmd, v.key, *ps, v.want)
			}
		} else if v.want != "" {
			t.Errorf("f.FindArg(%d, %v) == %q, want: %q", v.cmd, v.key, *ps, v.want)
		}
	}
}
